﻿using UnityEngine;

/// <summary>
/// Для контроля порядка инициализации.
/// </summary>
public class ScriptLoader : MonoBehaviour {

    private void Start() {
        GamePlayView.Instance.enabled = true;
        GamePlayController.Instance.enabled = true;
        AIPlayer.Instance.enabled = true;
    }

}
