﻿using UnityEngine;
using System.Collections.Generic;

public class AIPlayer : MonoBehaviour {

    private Difficulty _currentDifficulty;

    public static AIPlayer Instance;

    private void Awake() {
        Instance = this;
    }

    private void Start() {
        Init();
    }

    private void Init() {
        _currentDifficulty = PlayerData.LastSelectedDifficulty;
    }

    public Vector2Int MakeMove() {
        switch(_currentDifficulty) {
            case Difficulty.Easy:
            return MakeEasyMove();
            case Difficulty.Medium:
            return MakeMediumMove();
            case Difficulty.Invincible:
            return MakeInvincibleMove();
            default:
            return null;
        }
    }

    /// <summary>
    /// Просто ходит в случайную клетку.
    /// </summary>
    private Vector2Int MakeEasyMove() {
        var poss = GamePlayModel.Instance.GetFreeGameFieldCells();
        if(poss == null) {
            return null;
        }
        return poss[Random.Range(0, poss.Length)];
    }

    /// <summary>
    /// АИ играет в защиту и при первой возможности выигрывает, в остальных случаях ходит случайным образом.
    /// </summary>
    private Vector2Int MakeMediumMove() {
        var gameField = GamePlayModel.Instance.GetGameField();
        var sign = GamePlayModel.Instance.AICurrentSign;
        var playerSign = GamePlayModel.Instance.PlayerCurrentSign;

        #region One Move To Win
        // проверяем, можно ли завершить игру в один ход

        // Считаем суммы по горизонтали.
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            var rowSumm = 0;
            for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                rowSumm += gameField[i, j];
            }
            //  в строке уже стоит два его знака.
            if(rowSumm == sign * 2) {
                // АИ ходит последню пустую клетку и выигрывает.
                for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                    if(gameField[i, j] == 0) { //пустая клетка
                        return new Vector2Int(i, j);
                    }
                }
            }
        }
        // Считаем суммы по вертикали.
        for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
            var rowSumm = 0;
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                rowSumm += gameField[i, j];
            }
            // столбце уже стоит два его знака.
            if(rowSumm == sign * 2) {
                // АИ ходит последню пустую клетку и выигрывает.
                for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                    if(gameField[i, j] == 0) { //пустая клетка
                        return new Vector2Int(i, j);
                    }
                }
            }
        }


        // Считаем суммы по диагоналям.
        var diagSumm = 0;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            diagSumm += gameField[i, i];
        }
        if(diagSumm == sign * 2) {
            // АИ ходит последню пустую клетку и выигрывает.
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                if(gameField[i, i] == 0) { //пустая клетка
                    return new Vector2Int(i, i);
                }
            }
        }
        diagSumm = 0;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            diagSumm += gameField[GamePlayModel.FIELD_SIZE - 1 - i, i];
        }
        if(diagSumm == sign * 2) {
            // АИ ходит последню пустую клетку и выигрывает.
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                if(gameField[GamePlayModel.FIELD_SIZE - 1 - i, i] == 0) { //пустая клетка
                    return new Vector2Int(GamePlayModel.FIELD_SIZE - 1 - i, i);
                }
            }
        }


        #endregion

        #region One Move To Lose

        // проверяем, можно ли пригратьв один ход
        // если да, то АИ играет в защиту.

        // Считаем суммы по горизонтали.
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            var rowSumm = 0;
            for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                rowSumm += gameField[i, j];
            }
            // в строке уже стоит два знака игрока.
            if(rowSumm == playerSign * 2) {
                // АИ ходит в последню пустую клетку.
                for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                    if(gameField[i, j] == 0) { //пустая клетка
                        Debug.Log("АИ ходит в последню пустую клетку в строке для защиты");
                        return new Vector2Int(i, j);
                    }
                }
            }
        }
        // Считаем суммы по вертикали.
        for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
            var collSumm = 0;
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                collSumm += gameField[i, j];
            }
            //  в столбце уже стоит два знака игрока.
            if(collSumm == playerSign * 2) {
                // АИ ходит последню пустую клетку.
                for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                    if(gameField[i, j] == 0) { //пустая клетка
                        Debug.Log("АИ ходит в последню пустую клетку в столбце для защиты");
                        return new Vector2Int(i, j);
                    }
                }
            }
        }

        // Считаем суммы по диагоналям.
        diagSumm = 0;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            diagSumm += gameField[i, i];
        }
        if(diagSumm == playerSign * 2) {
            // АИ ходит последню пустую клетку.
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                if(gameField[i, i] == 0) { //пустая клетка
                    Debug.Log("АИ ходит в последню пустую клетку в диагонали для защиты");
                    return new Vector2Int(i, i);
                }
            }
        }
        diagSumm = 0;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            diagSumm += gameField[GamePlayModel.FIELD_SIZE - 1 - i, i];
        }
        if(diagSumm == playerSign * 2) {
            // АИ ходит последню пустую клетку.
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                if(gameField[GamePlayModel.FIELD_SIZE - 1 - i, i] == 0) { //пустая клетка
                    Debug.Log("АИ ходит в последню пустую клетку в диагонали для защиты");
                    return new Vector2Int(GamePlayModel.FIELD_SIZE - 1 - i, i);
                }
            }
        }


        #endregion



        Debug.Log("АИ ходит случайным образом");
        var poss = GamePlayModel.Instance.GetFreeGameFieldCells();
        return poss[Random.Range(0, poss.Length)];
    }

    /// <summary>
    /// Пытается ходить правильно. Если АИ играет ха "Х" то в лучшем случае можно сыграть в ничию.
    /// </summary>
    private Vector2Int MakeInvincibleMove() {
        var gameField = GamePlayModel.Instance.GetGameField();
        var sign = GamePlayModel.Instance.AICurrentSign;
        var playerSign = GamePlayModel.Instance.PlayerCurrentSign;

        #region One Move To Win
        // проверяем, можно ли завершить игру в один ход

        // Считаем суммы по горизонтали.
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            var rowSumm = 0;
            for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                rowSumm += gameField[i, j];
            }
            //  в строке уже стоит два его знака.
            if(rowSumm == sign * 2) {
                // АИ ходит последню пустую клетку и выигрывает.
                for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                    if(gameField[i, j] == 0) { //пустая клетка
                        Debug.Log("АИ собирается выиграть в один ход");
                        return new Vector2Int(i, j);
                    }
                }
            }
        }
        // Считаем суммы по вертикали.
        for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
            var rowSumm = 0;
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                rowSumm += gameField[i, j];
            }
            // столбце уже стоит два его знака.
            if(rowSumm == sign * 2) {
                // АИ ходит последню пустую клетку и выигрывает.
                for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                    if(gameField[i, j] == 0) { //пустая клетка
                        Debug.Log("АИ собирается выиграть в один ход");
                        return new Vector2Int(i, j);
                    }
                }
            }
        }


        // Считаем суммы по диагоналям.
        var diagSumm = 0;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            diagSumm += gameField[i, i];
        }
        if(diagSumm == sign * 2) {
            // АИ ходит последню пустую клетку и выигрывает.
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                if(gameField[i, i] == 0) { //пустая клетка
                    Debug.Log("АИ собирается выиграть в один ход");
                    return new Vector2Int(i, i);
                }
            }
        }
        diagSumm = 0;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            diagSumm += gameField[GamePlayModel.FIELD_SIZE - 1 - i, i];
        }
        if(diagSumm == sign * 2) {
            // АИ ходит последню пустую клетку и выигрывает.
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                if(gameField[GamePlayModel.FIELD_SIZE - 1 - i, i] == 0) { //пустая клетка
                    Debug.Log("АИ собирается выиграть в один ход");
                    return new Vector2Int(GamePlayModel.FIELD_SIZE - 1 - i, i);
                }
            }
        }


        #endregion

        #region One Move To Lose

        // проверяем, можно ли пригратьв один ход
        // если да, то АИ играет в защиту.

        // Считаем суммы по горизонтали.
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            var rowSumm = 0;
            for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                rowSumm += gameField[i, j];
            }
            // в строке уже стоит два знака игрока.
            if(rowSumm == playerSign * 2) {
                // АИ ходит в последню пустую клетку.
                for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                    if(gameField[i, j] == 0) { //пустая клетка
                        Debug.Log("АИ ходит в последню пустую клетку в строке для защиты");
                        return new Vector2Int(i, j);
                    }
                }
            }
        }
        // Считаем суммы по вертикали.
        for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
            var collSumm = 0;
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                collSumm += gameField[i, j];
            }
            //  в столбце уже стоит два знака игрока.
            if(collSumm == playerSign * 2) {
                // АИ ходит последню пустую клетку.
                for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                    if(gameField[i, j] == 0) { //пустая клетка
                        Debug.Log("АИ ходит в последню пустую клетку в столбце для защиты");
                        return new Vector2Int(i, j);
                    }
                }
            }
        }

        // Считаем суммы по диагоналям.
        diagSumm = 0;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            diagSumm += gameField[i, i];
        }
        if(diagSumm == playerSign * 2) {
            // АИ ходит последню пустую клетку.
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                if(gameField[i, i] == 0) { //пустая клетка
                    Debug.Log("АИ ходит в последню пустую клетку в диагонали для защиты");
                    return new Vector2Int(i, i);
                }
            }
        }
        diagSumm = 0;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            diagSumm += gameField[GamePlayModel.FIELD_SIZE - 1 - i, i];
        }
        if(diagSumm == playerSign * 2) {
            // АИ ходит последню пустую клетку.
            for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
                if(gameField[GamePlayModel.FIELD_SIZE - 1 - i, i] == 0) { //пустая клетка
                    Debug.Log("АИ ходит в последню пустую клетку в диагонали для защиты");
                    return new Vector2Int(GamePlayModel.FIELD_SIZE - 1 - i, i);
                }
            }
        }


        #endregion


        // если АИ ходит первым
        if(sign == 1) {
            // первый ход, центр пуст, ходим в него
            if(gameField[1, 1] != sign) {
                Debug.Log("АИ начинает с центра");
                return new Vector2Int(1, 1);
            }

            // если нолики походили в угол, АИ ходит в противоположный
            if(gameField[0, 0] == playerSign && gameField[2, 2] == 0) {
                Debug.Log("АИ ходит в противоположный угол");
                return new Vector2Int(2, 2);
            }
            if(gameField[0, 2] == playerSign && gameField[2, 0] == 0) {
                Debug.Log("АИ ходит в противоположный угол");
                return new Vector2Int(2, 0);
            }
            if(gameField[2, 0] == playerSign && gameField[0, 2] == 0) {
                Debug.Log("АИ ходит в противоположный угол");
                return new Vector2Int(0, 2);
            }
            if(gameField[2, 2] == playerSign && gameField[0, 0] == 0) {
                Debug.Log("АИ ходит в противоположный угол");
                return new Vector2Int(0, 0);
            }

        } else if(sign == -1) {// если АИ ходит вторым

            // если в центре крестик, АИ хожит в любой угол
            if(gameField[1, 1] == playerSign) {
                var corner = new List<Vector2Int>();
                if(gameField[0, 0] == 0) {
                    corner.Add(new Vector2Int(0, 0));
                }

                if(gameField[0, 2] == 0) {
                    corner.Add(new Vector2Int(0, 2));
                }
                if(gameField[2, 0] == 0) {
                    corner.Add(new Vector2Int(2, 0));
                }
                if(gameField[2, 2] == 0) {
                    corner.Add(new Vector2Int(2, 2));
                }

                if(corner.Count > 0) {
                    return corner[Random.Range(0, corner.Count)];
                }  //если свободных углов нет, то в любую клетку
            } else { //любом другом случает АИ занимает центр
                if(gameField[1, 1] == 0) {
                    return new Vector2Int(1, 1);
                }
            }

        }



        Debug.Log("АИ ходит случайным образом");
        var poss = GamePlayModel.Instance.GetFreeGameFieldCells();
        return poss[Random.Range(0, poss.Length)];
    }



}
