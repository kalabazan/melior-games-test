﻿using UnityEngine;



public class GamePlayController : MonoBehaviour {

    public static GamePlayController Instance;

    private GamePlayStateMachine _gamePlayStateMachine;

    public static bool GameEnded;

    private void Awake() {
        Instance = this;
    }

    private void Start() {
        _gamePlayStateMachine = new GamePlayStateMachine();
        _gamePlayStateMachine.AddState(new PlayerMove());
        _gamePlayStateMachine.AddState(new AIMove());
        Init();
    }

    public void Init() {
        _gamePlayStateMachine.Init(PlayerData.LastPlayerSign == 1 ? GameStates.PlayerMove : GameStates.AIMove);
    }


    public void CheckWinConditionsForSign(int sign) {
        var field = GamePlayModel.Instance.GetGameField();
        var winFlag = false;

        winFlag |= field[0, 0] == sign && field[0, 1] == sign && field[0, 2] == sign;
        winFlag |= field[1, 0] == sign && field[1, 1] == sign && field[1, 2] == sign;
        winFlag |= field[2, 0] == sign && field[2, 1] == sign && field[2, 2] == sign;

        winFlag |= field[0, 0] == sign && field[1, 0] == sign && field[2, 0] == sign;
        winFlag |= field[0, 1] == sign && field[1, 1] == sign && field[2, 1] == sign;
        winFlag |= field[0, 2] == sign && field[1, 2] == sign && field[2, 2] == sign;

        winFlag |= field[0, 0] == sign && field[1, 1] == sign && field[2, 2] == sign;
        winFlag |= field[0, 2] == sign && field[1, 1] == sign && field[2, 0] == sign;

        var ret = false;
        if(winFlag) {
            Debug.Log(sign == -1 ? "O Win" : "X Win");
            GamePlayView.Instance.GameOverSequence(sign);
            GameOverSequence(sign);
            ret = true;
        }

        if(!winFlag && GamePlayModel.Instance.GetFreeGameFieldCells().Length == 0) {
            Debug.Log("DRAW");
            GamePlayView.Instance.GameOverSequence(0);
            GameOverSequence(0);
            ret = true;
        }
        if(ret) {
            GamePlayView.Instance.DiableFieldButtons();
        }

        GameEnded = ret;

    }

    public void FieldStateChange(GameObject buttonClicked) {
        var btnPosString = buttonClicked.name.Replace("Button", "");
        var btnPos = new Vector2Int(int.Parse(btnPosString.Substring(0, 1)), int.Parse(btnPosString.Substring(1, 1)));

        GamePlayModel.Instance.UpdateGameFieldAt(btnPos, GamePlayModel.Instance.PlayerCurrentSign);
        GamePlayView.Instance.UpdateFieldButtonAt(btnPos);

        _gamePlayStateMachine.ChangeState(GameStates.AIMove);
    }

    private void GameOverSequence(int winnerSign) {
        if(winnerSign == 0) {
            GamePlayModel.Instance.PlayerCurrentSign *= -1;
            PlayerData.DrawCount++;
        } else {
            var isPlayerWin = winnerSign == GamePlayModel.Instance.PlayerCurrentSign;
            if(isPlayerWin) {
                PlayerData.PlayerWins++;
            } else {
                PlayerData.AIWins++;
            }
            GamePlayModel.Instance.PlayerCurrentSign = isPlayerWin ? 1 : -1;
        }

        PlayerData.LastPlayerSign = GamePlayModel.Instance.PlayerCurrentSign;
        GamePlayModel.Instance.GameOverSequence();

    }


}


