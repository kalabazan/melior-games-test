﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayView : MonoBehaviour {

    public static GamePlayView Instance;

    private Button _backButton;

    private Button _restartButton;

    private Button[,] _gameFieldButtons = new Button[3, 3];
    private Text[,] _gameFieldButtonsText = new Text[3, 3];

    private Text _playerScoreText;
    private Text _AIScoreText;
    private Text _drawScoreText;
    private Text _gameOverText;

    private GameObject _gameOverPanel;

    private void Awake() {
        Instance = this;
    }

    private void Start() {
        InitUI();
    }

    private void InitUI() {
        _backButton = GameObject.Find("BackButton").GetComponent<Button>();
        _backButton.onClick.RemoveAllListeners();
        _backButton.onClick.AddListener(BackButtonOnClick);

        PopulateGameFieldButtonsAndTexts();
        PopulateUITexts();
        PopulateGameOverUI();
    }


    private void PopulateGameOverUI() {
        _gameOverPanel = GameObject.Find("GameOverPanel");
        _gameOverText = GameObject.Find("GameOverText").GetComponent<Text>();
        _restartButton = GameObject.Find("RestartButton").GetComponent<Button>();
        _restartButton.onClick.RemoveAllListeners();
        _restartButton.onClick.AddListener(() => {
            InitUI();
            GamePlayController.Instance.Init();
        });
        _gameOverPanel.SetActive(false);
    }

    private void PopulateGameFieldButtonsAndTexts() {
        var gameFieldRoot = GameObject.Find("GameFieldRoot").transform;
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                var buttonGo = gameFieldRoot.FindChild(string.Format("Button{0}{1}", i, j)).gameObject;
                _gameFieldButtons[i, j] = buttonGo.GetComponent<Button>();
                _gameFieldButtons[i, j].interactable = true;
                _gameFieldButtons[i, j].onClick.RemoveAllListeners();
                _gameFieldButtons[i, j].onClick.AddListener(FieldButtonOnClick);
                _gameFieldButtonsText[i, j] = buttonGo.GetComponentInChildren<Text>();
                _gameFieldButtonsText[i, j].text = string.Empty;
            }
        }
    }

    private void PopulateUITexts() {
        _playerScoreText = GameObject.Find("PlayerScoreText").GetComponent<Text>();
        _AIScoreText = GameObject.Find("AIScoreText").GetComponent<Text>();
        _drawScoreText = GameObject.Find("DrawScoreText").GetComponent<Text>();

        _playerScoreText.text = PlayerData.PlayerWins.ToString();
        _AIScoreText.text = PlayerData.AIWins.ToString();
        _drawScoreText.text = PlayerData.DrawCount.ToString();
    }


    public void DiableFieldButtons() {
        for(int i = 0; i < GamePlayModel.FIELD_SIZE; i++) {
            for(int j = 0; j < GamePlayModel.FIELD_SIZE; j++) {
                _gameFieldButtons[i, j].interactable = false;
            }
        }
    }
    public void EnableFieldButtons() {
        var freeButtons = GamePlayModel.Instance.GetFreeGameFieldCells();
        for(int i = 0; i < freeButtons.Length; i++) {
            _gameFieldButtons[freeButtons[i].x, freeButtons[i].y].interactable = true;
        }
    }

    private void FieldButtonOnClick() {
        GamePlayController.Instance.FieldStateChange(EventSystem.current.currentSelectedGameObject);
    }

    /// <summary>
    /// Обновляет состояние кнопки с нужными индексами.
    /// </summary>
    /// <param name="pos"></param>
    public void UpdateFieldButtonAt(Vector2Int pos) {
        _gameFieldButtons[pos.x, pos.y].interactable = false;
        _gameFieldButtonsText[pos.x, pos.y].text = GetPlayerSign(GamePlayModel.Instance.GetGameFieldAt(pos));
    }

    public void GameOverSequence(int winnerSign) {
        _gameOverPanel.SetActive(true);

        if(winnerSign == 0) {
            _gameOverText.text = "GAME OVER\nDRAW";
            return;
        }
        var isPlayerWin = winnerSign == GamePlayModel.Instance.PlayerCurrentSign;
        _gameOverText.text = isPlayerWin
            ? string.Format("GAME OVER\nPlayer win ({0})", GetPlayerSign(winnerSign))
            : string.Format("GAME OVER\nAI win ({0})", GetPlayerSign(winnerSign));
    }


    private string GetPlayerSign(int player) {
        switch(player) {
            case -1:
            return "O";
            case 1:
            return "X";
            default:
            return string.Empty;
        }
    }


    private void BackButtonOnClick() {
        SceneManager.LoadScene(0);
    }


}
