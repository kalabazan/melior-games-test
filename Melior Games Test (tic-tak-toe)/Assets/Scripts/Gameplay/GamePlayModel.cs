﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс для удобного хранения индексовю
/// </summary>
public class Vector2Int {

    public Vector2Int(int x = 0, int y = 0) {
        this.x = x;
        this.y = y;
    }
    public int x;
    public int y;
    
    public override string ToString() {
        return string.Format("({0},{1})", x, y);
    }
}

public class GamePlayModel : MonoBehaviour {

    public static GamePlayModel Instance;

    /// <summary>
    /// Матрица игрового поля.
    /// </summary>
    private int[,] _gameField = new int[3,3]; // -1 = O, 0 = empty, 1 = X

    /// <summary>
    /// Размерность игрового поля.
    /// </summary>
    public const int FIELD_SIZE = 3;

    /// <summary>
    /// Знак игрока
    /// </summary>
    [HideInInspector]
    public int PlayerCurrentSign = 1;
    /// <summary>
    /// Просто всегда противоположен знаку игрока.
    /// </summary>
    public int AICurrentSign {
        get {
            return PlayerCurrentSign * -1;
        }
    }


    private void Awake() {
        Instance = this;
        PlayerCurrentSign = PlayerData.LastPlayerSign;
    }

    /// <summary>
    /// Обновляет состояние ячейки игрового поля.
    /// </summary>
    public void UpdateGameFieldAt(Vector2Int pos, int newState) {
        _gameField[pos.x, pos.y] = newState;
    }

    /// <summary>
    /// Возвращает состояние ячейки игрового поля.
    /// </summary>
    public int GetGameFieldAt(Vector2Int pos) {
        return _gameField[pos.x, pos.y];
    }

    /// <summary>
    /// Возвращает матрицу игрового поля целиком.
    /// </summary>
    public int[,] GetGameField() {
        return _gameField;
    }

    /// <summary>
    /// Возвращает массив позицый свобоний ячеек.
    /// </summary>
    /// <returns></returns>
    public Vector2Int[] GetFreeGameFieldCells() {
        var ret = new List<Vector2Int>();
        for(int i = 0; i < FIELD_SIZE; i++) {
            for(int j = 0; j < FIELD_SIZE; j++) {
                if(_gameField[i, j] == 0) {
                    ret.Add(new Vector2Int(i, j));
                }
            }
        }
        return ret.ToArray();
    }

    /// <summary>
    /// Зануляет игровое поле.
    /// </summary>
    public void GameOverSequence() {
        for(int i = 0; i < FIELD_SIZE; i++) {
            for(int j = 0; j < FIELD_SIZE; j++) {
                _gameField[i, j] = 0;
            }
        }

    }
}
