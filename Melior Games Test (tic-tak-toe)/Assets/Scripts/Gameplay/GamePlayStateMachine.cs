﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public enum GameStates {
    PlayerMove,
    AIMove
}

public class GamePlayStateMachine {
    private GamePlayState _currentGamePlayState;
    private List<GamePlayState> _statesList = new List<GamePlayState>();

    public void AddState(GamePlayState State) {
        State.GamePlayStateMachineInstance = this;
        _statesList.Add(State);
    }

    public void ChangeState(GameStates nextState) {
        if(_currentGamePlayState != null) {
            _currentGamePlayState.OnStateExit();
        }

        GamePlayController.Instance.CheckWinConditionsForSign(nextState == GameStates.AIMove ? GamePlayModel.Instance.PlayerCurrentSign :
                                                                                                                                   GamePlayModel.Instance.AICurrentSign);

        if(GamePlayController.GameEnded) {
            return;
        }
        _currentGamePlayState = _statesList.Select(x => x).Where(x => x.State == nextState).ToArray()[0];

        _currentGamePlayState.OnStateEnter();

        _currentGamePlayState.Work();
    }

    public void Init(GameStates startState) {
        ChangeState(startState);
    }

}


/// <summary>
/// Родительский класс состояния
/// </summary>
public abstract class GamePlayState {
    /// <summary>
    /// Название состояния
    /// </summary> 
    public GameStates State;
    /// <summary>
    /// Ссылка на саму стейт машину
    /// </summary> 
    public GamePlayStateMachine GamePlayStateMachineInstance;
    public abstract void OnStateEnter();
    public abstract void OnStateExit();
    public abstract void Work();
}

public class PlayerMove : GamePlayState {

    public PlayerMove() {
        State = GameStates.PlayerMove;
    }

    public override void Work() {

    }

    public override void OnStateEnter() {
        //Debug.Log("PlayerMove ENTER");
        GamePlayView.Instance.EnableFieldButtons();
    }

    public override void OnStateExit() {
        //Debug.Log("PlayerMove EXIT");

    }
}

public class AIMove : GamePlayState {

    public AIMove() {
        State = GameStates.AIMove;
    }

    public override void Work() {

        var pos = AIPlayer.Instance.MakeMove();
        Debug.Log("AI MOVE " + pos);
        GamePlayModel.Instance.UpdateGameFieldAt(pos, GamePlayModel.Instance.AICurrentSign);
        GamePlayView.Instance.UpdateFieldButtonAt(pos);

        GamePlayStateMachineInstance.ChangeState(GameStates.PlayerMove);

    }

    public override void OnStateEnter() {
        //Debug.Log("AIMove ENTER");
    }

    public override void OnStateExit() {
        //Debug.Log("AIMove EXIT");

    }
}