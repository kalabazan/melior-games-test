﻿using UnityEngine;


public enum Difficulty {
    Easy,
    Medium,
    Invincible
}

/// <summary>
/// Класс для хранения данных между сценами и сессиями.
/// </summary>
public class PlayerData : MonoBehaviour {

    /// <summary>
    /// Выбранный уровень сложности.
    /// </summary>
    public static Difficulty LastSelectedDifficulty {
        get {
            return (Difficulty)PlayerPrefs.GetInt("LastSelectedDifficulty", 0);
        }
        set {
            PlayerPrefs.SetInt("LastSelectedDifficulty", (int)value);
        }
    }
    /// <summary>
    /// Общее количество побед игрока.
    /// </summary>
    public static int PlayerWins {
        set {
            PlayerPrefs.SetInt("PlayerWins", value);
        }
        get {
            return PlayerPrefs.GetInt("PlayerWins", 0);
        }
    }
    /// <summary>
    /// Общее количество побед АИ.
    /// </summary>
    public static int AIWins {
        set {
            PlayerPrefs.SetInt("AIWins", value);
        }
        get {
            return PlayerPrefs.GetInt("AIWins", 0);
        }
    }
    /// <summary>
    /// Общее количество игр в ничию.
    /// </summary>
    public static int DrawCount {
        set {
            PlayerPrefs.SetInt("DrawCount", value);
        }
        get {
            return PlayerPrefs.GetInt("DrawCount", 0);
        }
    }

    /// <summary>
    /// Последний знак которым играл игрок.
    /// </summary>
    public static int LastPlayerSign {
        set {
            PlayerPrefs.SetInt("LastPlayerSign", value);
        }
        get {
            return PlayerPrefs.GetInt("LastPlayerSign", 1);
        }
    }


}
