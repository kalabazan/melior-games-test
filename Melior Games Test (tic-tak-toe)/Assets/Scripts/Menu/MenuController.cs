﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    private MenuModel _menuModel;

    private Dropdown _difficultyDropdown;
    private Button _startButton;

	private void Start ()
	{
	    _menuModel = FindObjectOfType<MenuModel>();

	    _difficultyDropdown = GameObject.Find("DifficultyDropdown").GetComponent<Dropdown>();
        _difficultyDropdown.value = (int) _menuModel.SelectedDifficulty;
        _difficultyDropdown.onValueChanged.AddListener(DifficultyDropdownOnValueChanged);

        _startButton = GameObject.Find("StartButton").GetComponent<Button>();
        _startButton.onClick.AddListener(StartButtonOnClick);
	}


    private void DifficultyDropdownOnValueChanged(int value)
    {
        _menuModel.SelectedDifficulty = (Difficulty)value;
    }

    private void StartButtonOnClick() {
        SceneManager.LoadScene(1);
    }


}
