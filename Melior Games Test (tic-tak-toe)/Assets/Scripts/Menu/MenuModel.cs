﻿using UnityEngine;


public class MenuModel : MonoBehaviour
{

    private Difficulty _selecteDifficulty;
    public Difficulty SelectedDifficulty {
        get { return _selecteDifficulty; }
        set
        {
            _selecteDifficulty = value;
            PlayerData.LastSelectedDifficulty = _selecteDifficulty;
        }
    }


    private void Start()
    {
        _selecteDifficulty = PlayerData.LastSelectedDifficulty;
    }

}
